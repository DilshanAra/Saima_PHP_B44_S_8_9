<?php
class person{
        public $name;
        public $birth;
        public function setname($pname)
        {
            $this->name = $pname;
        }
    //if inside class ($name) then use (this), or if outside class use (obj)
        public function setbirth($dbirth)
        {
            $this->birth = $dbirth;
        }
    //use 2 underscore
    public function __construct()
    {
        echo"I am inside construct<br>";
    }
    public function __destruct()
    {
        echo "maf kore dien<br>";
    }

}
//this name and other name is not same, so the value of name is empty
class student extends person{
        public function dosomething()
        {
            echo $this->name . "<br>";
        }
}
$objstudent = new student();
$objstudent->setname("rohim");
echo $objstudent->dosomething();
$objperson = new person();
$objperson->setname("jorina");
$objbekti = new person();
$objbekti->setname("kalam");
echo $objstudent->name . "<br>";
unset($objstudent);
echo $objbekti->name . "<br>";
unset($objbekti);
echo $objperson->name . "<br>";